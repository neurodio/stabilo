﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameMenu : MonoBehaviour
{

    public static InGameMenu instance;

    public CanvasGroup gamePanel;
    public CanvasGroup pausePanel;

    [Space(10)]
    public GameObject holdPosPanel;
    public GameObject landscapePanel;
    public GameObject forestPanel;

    [Space(10)]
    public GameObject tutorialPanel;

    [Header("Win/Loose panels")]
    public CanvasGroup winPanel;
    public CanvasGroup loosePanel;
    public Text pointsText;
    public GameObject recordGo;

    public Image LandscapeWinPanel;
    public Image LandscapeWinImage;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
            Destroy(gameObject);
    }

    public void OpenGamePanel_Anim()
    {
        AnimationHelper.instance.AnimateCanvas(Enums.CanvasAnimationDir.Open, gamePanel);
    }

    public void OpenPausePanel()
    {
        if (winPanel.gameObject.activeSelf == false && loosePanel.gameObject.activeSelf == false)
        {
            GameManager.instance.PauseGame();
            pausePanel.gameObject.SetActive(true);
            pausePanel.alpha = 1;
        }
    }

    public void OpenPausePanel_Anim()
    {
        if (winPanel.gameObject.activeSelf == false && loosePanel.gameObject.activeSelf == false)
        {
            GameManager.instance.PauseGame();
            AnimationHelper.instance.AnimateCanvas(Enums.CanvasAnimationDir.Open, pausePanel);
        }
    }

    public void ClosePausePanel()
    {
        pausePanel.gameObject.SetActive(false);
        GameManager.instance.UnpauseGame();
    }

    public void ClosePausePanel_Anim()
    {
        AnimationHelper.instance.AnimateCanvas(Enums.CanvasAnimationDir.Close, pausePanel);
        GameManager.instance.UnpauseGame();
    }

    internal void OpenGamePanel(Enums.GameType currentGameType)
    {
        switch (currentGameType)
        {
            case Enums.GameType.HoldInPlace:
                holdPosPanel.SetActive(true);
                landscapePanel.SetActive(false);
                forestPanel.SetActive(false);
                break;
            case Enums.GameType.Landscape:
                holdPosPanel.SetActive(false);
                landscapePanel.SetActive(true);
                forestPanel.SetActive(false);
                break;
            case Enums.GameType.Forest:
                holdPosPanel.SetActive(false);
                landscapePanel.SetActive(false);
                forestPanel.SetActive(true);
                break;
        }
        HideWinLoosePanels();
    }

    internal void UpdateLifePanel(int playerCurrentLife, int playerLife)
    {
        throw new NotImplementedException();
    }

    internal void ShowWinPanel(int points)
    {
        if (GameManager.instance.currentGameType == Enums.GameType.Landscape)
        {
            Texture2D tex = LandscapeLevelBuilder.instance.GetCurrentImage();
            LandscapeWinImage.sprite = Sprite.Create(tex,new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
            LandscapeWinPanel.gameObject.SetActive(true);
        }
        else
        {
            LandscapeWinPanel.gameObject.SetActive(false);
        }

        // wyniki i rekord

        bool record = false;
        switch (GameManager.instance.currentGameType)
        {
            case Enums.GameType.HoldInPlace:
                pointsText.text = "Twój wynik: " + points + "\nRekord: " + GameManager.instance.holdRecord;
                if (points >= GameManager.instance.holdRecord)
                    record = true;
                break;
            case Enums.GameType.Landscape:
                pointsText.text = "Twój wynik: " + points + "\nRekord: " + GameManager.instance.landscapeRecord;
                if (points >= GameManager.instance.landscapeRecord)
                    record = true;
                break;
            case Enums.GameType.Forest:
                pointsText.text = "Twój wynik: " + points + "\nRekord: " + GameManager.instance.forestRecord;
                if (points >= GameManager.instance.forestRecord)
                    record = true;
                break;
        }
        if(record)
        {
            recordGo.SetActive(true);
        }
        else
        {
            recordGo.SetActive(false);
        }


        SoundManager.instance.PlayOnShowWinSfx();
        GameManager.instance.currentGameState = Enums.GameState.GameOver;
        loosePanel.gameObject.SetActive(false);
        AnimationHelper.instance.AnimateCanvas(Enums.CanvasAnimationDir.Open, winPanel);
    }

    internal void ShowLoosePanel()
    {
        SoundManager.instance.PlayOnShowWinSfx();
        GameManager.instance.currentGameState = Enums.GameState.GameOver;
        winPanel.gameObject.SetActive(false);
        AnimationHelper.instance.AnimateCanvas(Enums.CanvasAnimationDir.Open, loosePanel);
    }

    internal void HideWinLoosePanels()
    {
        winPanel.gameObject.SetActive(false);
        loosePanel.gameObject.SetActive(false);
    }

    public void ShowTutorialPanel()
    {
        tutorialPanel.SetActive(true);
    }

    public void HideTutorialPanel()
    {
        TutorialManager.instance.CloseAllTutorials();
        tutorialPanel.SetActive(false);
    }
}
