﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    [Header("Game state")]
    public Enums.GameState currentGameState = Enums.GameState.MainMenu;
    public Enums.GameType currentGameType;
    [Range(1, 9)]
    public int currentStressValue = 1; //[1-9]
    [HideInInspector]
    public GameObject currentGameObject;
    public bool gameIsPaused = false;
    public int playerLife = 500;
    private int playerCurrentLife = 500;

    [Header("Game settings")]
    public CameraFollow cameraFollow;
    public float countdownSecounds = 3;
    public float changeSongTimer = 2;

    [Header("Game music")]
    public AudioClip menuSong;
    public AudioClip holdInPlaceSong;
    public AudioClip landscapeSong;
    public AudioClip forestSong;

    [Header("Hold in place")]
    public GameObject holdInPlaceGO;
    public HoldPositionGUI holdPosGUI;
    public List<float> holdInPlaceDifficultyParams;


    [Header("LandscapeReveal")]
    public GameObject lanscapeRevealGO;
    public LandscapeRevealGUI lanscapeRevealGUI;

    [Header("Forest")]
    public GameObject ForestGO;
    public ForestGUI ForestGUI;

    [Header("Records")]
    public int holdRecord = 0;
    public int landscapeRecord = 0;
    public int forestRecord = 0;
    public bool deleteRecords = false;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
            Destroy(gameObject);

        if (deleteRecords)
        {
            PlayerPrefs.DeleteAll();
            holdRecord = 0;
            landscapeRecord = 0;
            forestRecord = 0;
        }
    }

    private void Start()
    {
        ReadRecords();
    }

    #region PlayerLife
    public void Update()
    {
        if (playerCurrentLife <= 0 && currentGameState == Enums.GameState.Level)
        {
            // game over
            InGameMenu.instance.ShowLoosePanel();
            currentGameState = Enums.GameState.GameOver;
        }
    }

    public void GetHit(int hit)
    {
        if (gameIsPaused == false)
        {

            playerCurrentLife -= hit;
            if (playerCurrentLife < 0)
                playerCurrentLife = 0;

            UpdateLifePanel();
        }
    }

    private void UpdateLifePanel()
    {
        float life = 1;
        life = (float)playerCurrentLife / (float)playerLife;
        //Debug.Log("life: " + life);
        holdPosGUI.SetLife(life);
        lanscapeRevealGUI.SetProgressBar(life);
        ForestGUI.SetProgressBar(life);
    }
    #endregion PlayerLife

    #region Pause
    public void PauseGame()
    {
        gameIsPaused = true;
        Time.timeScale = 0;
    }

    public void UnpauseGame()
    {
        gameIsPaused = false;
        Time.timeScale = 1;
    }
    #endregion Pause

    internal void SetStressValue(float value)
    {
        currentStressValue = (int)value;
    }

    internal void SetGameType(int value)
    {
        currentGameType = (Enums.GameType)value;
    }

    internal void StartNewGame()
    {
        Debug.Log("Start game: " + currentGameType + "  " + currentStressValue);

        playerCurrentLife = playerLife;
        holdPosGUI.SetLife(1);
        lanscapeRevealGUI.SetProgressBar(1);
        ForestGUI.SetProgressBar(1);

        currentGameState = Enums.GameState.Tutorial;
        GUIManager.instance.OpenInGamePanel();
        Destroy(currentGameObject);

        switch (currentGameType)
        {
            case Enums.GameType.HoldInPlace:
                StartHoldInPlace();
                break;
            case Enums.GameType.Landscape:
                StartLandscapeReveal();
                break;
            case Enums.GameType.Forest:
                StartForest();
                break;
        }
    }

    public void RestartGame()
    {
        InGameMenu.instance.HideWinLoosePanels();
        InGameMenu.instance.ClosePausePanel_Anim();
        DestroyImmediate(currentGameObject);
        Debug.Log("Dodać kod");
        StartNewGame();
    }

    void StartHoldInPlace()
    {
        cameraFollow.gameType = Enums.GameType.HoldInPlace;
        SoundManager.instance.ChangeSong(holdInPlaceSong);
        InGameMenu.instance.OpenGamePanel(currentGameType);
        currentGameObject = (GameObject)Instantiate(holdInPlaceGO);
        holdPosGUI.StartLevelGUI(holdInPlaceDifficultyParams[currentStressValue - 1]);
    }

    void StartLandscapeReveal()
    {
        cameraFollow.gameType = Enums.GameType.Landscape;
        SoundManager.instance.ChangeSong(landscapeSong);
        InGameMenu.instance.OpenGamePanel(currentGameType);
        currentGameObject = (GameObject)Instantiate(lanscapeRevealGO);
        lanscapeRevealGUI.StartLevelGUI();
    }

    void StartForest()
    {
        cameraFollow.gameType = Enums.GameType.Forest;
        SoundManager.instance.ChangeSong(forestSong);
        InGameMenu.instance.OpenGamePanel(currentGameType);
        currentGameObject = (GameObject)Instantiate(ForestGO);
        ForestGUI.StartLevelGUI();
    }

    /// <summary>
    /// Method called when player win or loose the game
    /// </summary>
    /// <param name="v">win/loose</param>
    public void GameIsOver(bool v)
    {
        Debug.Log("GameOver: " + v);
        SoundManager.instance.ChangeSong(menuSong);
        GUIManager.instance.OpenMainMenuPanel_Anim();
        currentGameState = Enums.GameState.MainMenu;
        Destroy(currentGameObject);
    }

    public void GoToEbook()
    {
        Application.OpenURL(@"https://drive.google.com/open?id=1obip_F4kWea2cfx_afmNR6FEbcfKQngA");
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("holdRecord", holdRecord);
        PlayerPrefs.SetInt("landscapeRecord", landscapeRecord);
        PlayerPrefs.SetInt("forestRecord", forestRecord);
    }

    private void ReadRecords()
    {
        holdRecord = PlayerPrefs.GetInt("holdRecord", 0);
        landscapeRecord = PlayerPrefs.GetInt("landscapeRecord", 0);
        forestRecord = PlayerPrefs.GetInt("forestRecord", 0);
    }

    public void HoldPosSetRecord(int points)
    {
        if (points > holdRecord)
            holdRecord = points;
    }

    public void LandscapeSetRecord(int points)
    {
        if (points > landscapeRecord)
            landscapeRecord = points;
    }

    public void ForestSetRecord(int points)
    {
        if (points > forestRecord)
            forestRecord = points;
    }
}
