﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputManager : MonoBehaviour
{

    public static InputManager instance;

    public CameraFollow cameraFollow;
    public Rigidbody rigid;
    public float forceMultiplier = 5;
    private bool gyroEnabled;
    private Gyroscope gyro;
    Vector2 normalizeGyro;
    private bool forceAcc = true;
    internal Vector3 currentVelocity;

    private float xRot;
    private float zRot;
    private float horiz;
    private float vert;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        Application.targetFrameRate = 60;
    }

    private void Start()
    {

        gyroEnabled = EnableGyro();
    }

    private bool EnableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;
            return true;
        }
        return false;
    }


    void FixedUpdate()
    {
        if (GameManager.instance.gameIsPaused == true || GameManager.instance.currentGameState != Enums.GameState.Level)
        {
            rigid.Sleep();
            return;
        }
        else
            rigid.WakeUp();

#if UNITY_EDITOR

        horiz = Input.GetAxis("Horizontal");
        vert = Input.GetAxis("Vertical");
        xRot = horiz * forceMultiplier;
        zRot = vert * forceMultiplier;

#elif UNITY_ANDROID || UNITY_IOS
        if (gyroEnabled && forceAcc == false)
        {
            normalizeGyro = GetNormalizeGyro();
            xRot = normalizeGyro.x * forceMultiplier;
            zRot = normalizeGyro.y * forceMultiplier;
        }
        else
        {
            xRot = Input.acceleration.x * forceMultiplier;
            zRot = Input.acceleration.y * forceMultiplier;
        }
#endif

        //xRot = xRot % forceMultiplier;
        //zRot = zRot % forceMultiplier;

        //cameraFollow.SetMoveOffset(new Vector3(horiz, 0, vert));

        switch (GameManager.instance.currentGameType)
        {
            case Enums.GameType.HoldInPlace:
                HoldPositionMove();
                break;
            case Enums.GameType.Landscape:
                LandscapeMove();
                break;
            case Enums.GameType.Forest:
                ForestMove();
                break;
        }

        currentVelocity = rigid.velocity;
    }

    void HoldPositionMove()
    {
        Vector3 sumOfForces = new Vector3(xRot, zRot, 0) + RandomForce.instance.GetRandomForceVector();
        rigid.AddForce(sumOfForces);
        //Debug.Log("SumOfForces: " + sumOfForces);
    }

    void LandscapeMove()
    {
        rigid.AddForce(new Vector3(xRot, zRot, 0));
    }

    void ForestMove()
    {
        rigid.AddForce(new Vector3(xRot, zRot, 0));
    }

    //private void OnGUI()
    //{
    //    GUIStyle gs = new GUIStyle();
    //    gs.fontSize = 30;
    //    gs.normal.textColor = Color.white;


    //    GUI.Label(new Rect(10, 10, 300, 100), "acc: " + Input.acceleration.ToString(), gs);
    //    if (gyroEnabled)
    //        GUI.Label(new Rect(10, 50, 300, 100), "gyro: " + normalizeGyro.x + " " + normalizeGyro.y); //GyroToUnity(gyro.attitude).eulerAngles, gs);
    //    else
    //        GUI.Label(new Rect(10, 50, 300, 100), "no gyro!", gs);



    //    if (GUI.Button(new Rect(10, 110, 300, 80), "RESTART"))
    //    {
    //        SceneManager.LoadScene("Main");
    //    }

    //    if (GUI.Button(new Rect(10, 250, 300, 80), "Gyro"))
    //    {
    //        forceAcc = false;
    //    }

    //    if (GUI.Button(new Rect(10, 350, 300, 80), "Acc"))
    //    {
    //        forceAcc = true;
    //    }
    //}

    private static Quaternion GyroToUnity(Quaternion q)
    {
        return new Quaternion(q.x, q.y, -q.z, -q.w);
    }

    private Vector2 GetNormalizeGyro()
    {
        Vector2 g;

        // zamienione osie
        float gyroY = GyroToUnity(gyro.attitude).eulerAngles.x * forceMultiplier;
        float gyroX = GyroToUnity(gyro.attitude).y * forceMultiplier;

        if (gyroY > 0 && gyroY < 90)
        {
            gyroY = Mathf.Clamp(gyroY, 0, 90);
            gyroY = ExtensionMethods.Remap(gyroY, 0, 90, 0, 1);
        }
        else
        {
            gyroY = Mathf.Clamp(gyroY, 270, 360);
            gyroY = ExtensionMethods.Remap(gyroY, 270, 360, -1, 0);
        }

        if (gyroX > 0 && gyroX < 90)
        {
            gyroX = Mathf.Clamp(gyroX, 0, 90);
            gyroX = ExtensionMethods.Remap(gyroX, 0, 90, 0, 1);
        }
        else
        {
            gyroX = Mathf.Clamp(gyroX, 270, 360);
            gyroX = ExtensionMethods.Remap(gyroX, 270, 360, -1, 0);
        }

        g.x = gyroX;
        g.y = gyroY;

        Debug.Log("Gyro: " + g);
        return g;
    }

}

public static class ExtensionMethods
{
    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}