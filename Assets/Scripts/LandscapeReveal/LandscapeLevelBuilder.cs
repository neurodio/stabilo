﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandscapeLevelBuilder : MonoBehaviour
{
    public static LandscapeLevelBuilder instance;

    public List<float> lanscapeRevealDifficultyParams;

    public GameObject landscapeBoardGO;
    public Material landscapeMaterial;
    public float lvlSize = 30;

    public List<Texture2D> textures;
    private Texture2D currentTex;

    [Space(10)]
    public Transform BlockHolder;
    public GameObject blockPrefab;

    [Space(10)]
    public Transform ball;
    public Transform colorBlockHolder;
    public GameObject colorCubePrefab;

    private List<Transform> blocks;
    int numberOfBlocks = -1;
    float offset = 0;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    void Start()
    {
        lvlSize = lanscapeRevealDifficultyParams[GameManager.instance.currentStressValue - 1];
        MaterialUpdate(0);
        CreateLevel();
    }

    private void Update()
    {
        AnimateBlocks();
    }

    void CreateLevel()
    {
        numberOfBlocks = -1;
        int rnd = UnityEngine.Random.Range(0, textures.Count);
        currentTex = textures[rnd];

        landscapeMaterial.mainTexture = currentTex;

        // scale
        Vector3 newScale = new Vector3(lvlSize, 1, lvlSize);
        float ratio = (float)currentTex.height / currentTex.width;
        newScale.z = Mathf.Round(newScale.x * ratio);
        landscapeBoardGO.transform.localScale = newScale;

        // spawn 
        SpawnBlocks(newScale);
    }

    private void SpawnBlocks(Vector3 newScale)
    {
        int x = (int)newScale.x;
        int z = (int)newScale.z;
        Vector3 blockScale = new Vector3(1 / (landscapeBoardGO.transform.localScale.x + 1), 1 / (landscapeBoardGO.transform.localScale.z + 1), 1);
        blocks = new List<Transform>();
        numberOfBlocks = x * z;

        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < z; j++)
            {
                GameObject go = Instantiate(blockPrefab, new Vector3(i, j, 0), Quaternion.identity);
                go.transform.parent = BlockHolder;
                blocks.Add(go.transform);
            }
        }

        BlockHolder.position = new Vector3((landscapeBoardGO.transform.localScale.x / -2) + 0.5f, (landscapeBoardGO.transform.localScale.z / -2) + 0.5f, -1.25f);

        // Spawn colorCubes
        for (int i = -1; i <= x; i++)
        {
            if (i == 0)
            {
                for (int j = 0; j < z; j++)
                {
                    GameObject go = Instantiate(colorCubePrefab, new Vector3(i - 2f, j, 0), Quaternion.identity);
                    go.transform.localScale = new Vector3(3, 1, 1);
                    go.GetComponent<ColorCube>().ballPos = ball;
                    go.transform.parent = colorBlockHolder;
                }
            }
            else if (i == x - 1)
            {
                for (int j = 0; j < z; j++)
                {
                    GameObject go = Instantiate(colorCubePrefab, new Vector3(i + 2f, j, 0), Quaternion.identity);
                    go.transform.localScale = new Vector3(3, 1, 1);
                    go.GetComponent<ColorCube>().ballPos = ball;
                    go.transform.parent = colorBlockHolder;
                }
            }

            Vector3 newCornerScale = new Vector3(1, 3, 1);
            float iOffset = 0;
            if (i == -1 || i == x)
            {
                newCornerScale = new Vector3(3, 3, 1);
                if (i == -1)
                    iOffset = -1;
                else
                    iOffset = 1;
            }

            {
                GameObject go = Instantiate(colorCubePrefab, new Vector3(i + iOffset, -2f, 0), Quaternion.identity);
                go.transform.localScale = newCornerScale;
                go.GetComponent<ColorCube>().ballPos = ball;
                go.transform.parent = colorBlockHolder;

                go = Instantiate(colorCubePrefab, new Vector3(i + iOffset, z + 1f, 0), Quaternion.identity);
                go.transform.localScale = newCornerScale;
                go.GetComponent<ColorCube>().ballPos = ball;
                go.transform.parent = colorBlockHolder;
            }
        }
        colorBlockHolder.position = new Vector3((landscapeBoardGO.transform.localScale.x / -2) + 0.5f, (landscapeBoardGO.transform.localScale.z / -2) + 0.5f, -1f);
    }

    internal Texture2D GetCurrentImage()
    {
        return currentTex;
    }

    public int GetNumberOfBlocks()
    {
        return numberOfBlocks;
    }

    public void BlockDestroyed()
    {
        numberOfBlocks--;
    }

    public void MaterialUpdate(float saturation)
    {
        landscapeMaterial.SetFloat("_Saturation", saturation);
        //Debug.Log("MatUpdate " + saturation);
    }

    void AnimateBlocks()
    {
        for (int i = 0; i < blocks.Count; i++)
        {
            if (blocks[i] != null)
            {
                float z = Mathf.Sin(blocks[i].position.x + blocks[i].position.y + offset) - 1;
                z *= 0.2f;
                //blocks[i].position = new Vector3(blocks[i].position.x, blocks[i].position.y, z);
                z += 1;
                blocks[i].localScale = new Vector3(z, z, z);
            }
        }
        offset += 0.01f;
    }

}
