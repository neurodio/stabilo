﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorCube : MonoBehaviour {

    public Transform ballPos;
    MeshRenderer meshRenderer;
    private float dist = 0;
    public Material spikesMat;
    public Color nearColor;
    public Color farColor;
    public float maxDistance;

    // Use this for initialization
    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.materials[0] = new Material(spikesMat);
    }

    // Update is called once per frame
    void Update()
    {
        dist = Vector3.Distance(this.transform.position, ballPos.position);
        meshRenderer.materials[0].color = Color.Lerp(nearColor, farColor, dist / maxDistance);
    }
}
