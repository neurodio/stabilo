﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockReveal : MonoBehaviour {

    bool destroyed = false;
    public GameObject particle;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player) && destroyed == false)
        {
            destroyed = true;
            GetComponent<SphereCollider>().enabled = false;
            Instantiate(particle, transform.position, Quaternion.identity, transform.parent);
            // sound
            SoundManager.instance.PlayOnPopSfx();
            // animation

            //Debug.Log("Trigger");
            LandscapeLevelBuilder.instance.BlockDestroyed();
            Destroy(this.gameObject);
        }
    }
}
