﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LandscapeRevealGUI : MonoBehaviour
{

    public Image ProgressImage;
    bool levelInProgress = false;

    int maxBlockCount = 0;

    public Text pointsText;

    internal void StartLevelGUI()
    {
        maxBlockCount = 0;
        pointsText.text = "0";
        levelInProgress = false;
        ShowTutorial();
    }

    private void ShowTutorial()
    {
        InGameMenu.instance.ShowTutorialPanel();
        TutorialManager.instance.ShowTutorial(Enums.GameType.Landscape);
    }

    public void HideTutorial()
    {
        Debug.Log("Hide tutorial - landscape");
        GameManager.instance.currentGameState = Enums.GameState.Level;
        Invoke("StartLevel", GameManager.instance.countdownSecounds);
    }

    private void StartLevel()
    {
        maxBlockCount = LandscapeLevelBuilder.instance.GetNumberOfBlocks();
        levelInProgress = true;
    }

    private void Update()
    {
        if (levelInProgress && GameManager.instance.gameIsPaused == false)
        {
            // odpytuj ile bloków zostało
            int blockCount = LandscapeLevelBuilder.instance.GetNumberOfBlocks();
            // wylicz proporcje
            float progress = (float)blockCount / (float)maxBlockCount;
            float points = (maxBlockCount - blockCount) * 10 * ((float)GameManager.instance.currentStressValue / 10f);
            pointsText.text = points.ToString();
            //Debug.Log("blockCount: " + blockCount + "  max: " + maxBlockCount + "  prog: " + progress);
            // setLife
            SetProgressBar(progress);
            LandscapeLevelBuilder.instance.MaterialUpdate(1 - progress);

            if (blockCount == 0)
            {
                GameManager.instance.LandscapeSetRecord((int)points);
                InGameMenu.instance.ShowWinPanel((int)points);
                levelInProgress = false;
            }
        }
    }

    internal void SetProgressBar(float life)
    {
        ProgressImage.fillAmount = life;
    }
}
