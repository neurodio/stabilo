﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    public static MainMenuManager instance;

    public CanvasGroup MainMenuFront;
    public CanvasGroup StressPanel;
    public CanvasGroup RecordPanel;
    public CanvasGroup WelcomePanel;
    public CanvasGroup InfoPanel;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        int i = PlayerPrefs.GetInt("FirstUse", 0);
        if(i == 0) // pierwsze użycie
        {
            OpenWelcomePanel();
        }
        else
        {
            WelcomePanel.gameObject.SetActive(false);
        }
    }

    public void OpenStressPanel(int gameId)
    {
        GameManager.instance.SetGameType(gameId);
        AnimationHelper.instance.AnimateCanvas(Enums.CanvasAnimationDir.Open, StressPanel);
    }

    public void CloseStressPanel_Anim()
    {
        AnimationHelper.instance.AnimateCanvas(Enums.CanvasAnimationDir.Close, StressPanel);
    }

    public void CloseStressPanel()
    {
        StressPanel.gameObject.SetActive(false);
    }

    public void OperRecordPanel()
    {
        AnimationHelper.instance.AnimateCanvas(Enums.CanvasAnimationDir.Open, RecordPanel);
    }

    public void CloseRecordPanel()
    {
        AnimationHelper.instance.AnimateCanvas(Enums.CanvasAnimationDir.Close, RecordPanel);
    }

    public void OpenWelcomePanel()
    {
        WelcomePanel.gameObject.SetActive(true);
        PlayerPrefs.SetInt("FirstUse", 1);
    }

    public void CloseWelcomePanel()
    {
        AnimationHelper.instance.AnimateCanvas(Enums.CanvasAnimationDir.Close, WelcomePanel);
    }

    public void OpenInfoPanel()
    {
        AnimationHelper.instance.AnimateCanvas(Enums.CanvasAnimationDir.Open, InfoPanel);
    }

    public void CloseInfoPanel()
    {
        AnimationHelper.instance.AnimateCanvas(Enums.CanvasAnimationDir.Close, InfoPanel);
    }
}
