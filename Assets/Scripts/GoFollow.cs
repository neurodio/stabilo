﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoFollow : MonoBehaviour
{

    public Transform goToFollow;
    public bool constantHeight = false;
    public float spriteHeight = -1;



    void Update()
    {
        if (GameManager.instance.gameIsPaused == true)
            return;

        Vector3 newPos = goToFollow.position;
        if (constantHeight == true)
            newPos.z = spriteHeight;
        transform.position = newPos;
    }
}
