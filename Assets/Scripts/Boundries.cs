﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Boundries
{

    [Header("Boundries")]
    public bool useBoundries;
    public Vector2 horizontalBoundries;
    public Vector2 verticalBoundries;
}
