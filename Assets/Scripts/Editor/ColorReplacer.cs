﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ColorReplacer : EditorWindow
{

    private const string EditorPrefsKey = "Utilities.ColorReplacer";
    private const string MenuItemName = "Utilities/Replace Colors...";

    private Color _src;
    private Color _dest;
    private bool _includePrefabs;

    [MenuItem(MenuItemName)]
    public static void DisplayWindow()
    {
        var window = GetWindow<ColorReplacer>(true, "Replace Color");
        var position = window.position;
        position.size = new Vector2(position.size.x, 151);
        position.center = new Rect(0f, 0f, Screen.currentResolution.width, Screen.currentResolution.height).center;
        window.position = position;
        window.Show();
    }

    public void OnEnable()
    {
        _includePrefabs = EditorPrefs.GetBool(EditorPrefsKey + ".includePrefabs", false);
    }

    public void OnGUI()
    {
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PrefixLabel("Find:");
        _src = EditorGUILayout.ColorField("Old Color", _src);

        EditorGUILayout.Space();
        EditorGUILayout.PrefixLabel("Replace with:");
        _dest = EditorGUILayout.ColorField("New Color", _dest);

        EditorGUILayout.Space();
        _includePrefabs = EditorGUILayout.ToggleLeft("Include Prefabs", _includePrefabs);
        if (EditorGUI.EndChangeCheck())
        {
            EditorPrefs.SetString(EditorPrefsKey + ".src", GetColorString(_src));
            EditorPrefs.SetString(EditorPrefsKey + ".dest", GetColorString(_dest));
            EditorPrefs.SetBool(EditorPrefsKey + ".includePrefabs", _includePrefabs);
        }

        GUI.color = Color.green;
        if (GUILayout.Button("Replace All", GUILayout.Height(EditorGUIUtility.singleLineHeight * 2f)))
        {
            ReplaceColor(_src, _dest, _includePrefabs);
        }
        GUI.color = Color.white;
    }

    private static void ReplaceColor(Color src, Color dest, bool includePrefabs)
    {
        var sceneMatches = 0;
        for (var i = 0; i < SceneManager.sceneCount; i++)
        {
            var scene = SceneManager.GetSceneAt(i);
            var gos = new List<GameObject>(scene.GetRootGameObjects());
            foreach (var go in gos)
            {
                //sceneMatches += ReplaceColor(src, dest, go.GetComponentsInChildren<Image>(true));
            }
        }

        if (includePrefabs)
        {
            var prefabMatches = 0;
            var prefabs = AssetDatabase.FindAssets("t:Prefab").Select(guid => AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(guid)));
            foreach (var prefab in prefabs)
            {
                //prefabMatches += ReplaceColor(src, dest, prefab.GetComponentsInChildren<Image>(true));
            }

            Debug.LogFormat("Replaced {0} font(s), {1} in scenes, {2} in prefabs", sceneMatches + prefabMatches, sceneMatches, prefabMatches);
        }
        else
        {
            Debug.LogFormat("Replaced {0} font(s) in scenes", sceneMatches);
        }
    }

    private static int ReplaceColor(Color src, Color dest, IEnumerable<Image> images)
    {
        var matches = 0;
        var imagesFiltered = src != null ? images.Where(image => image.color == src) : images;
        foreach (var image in imagesFiltered)
        {
            image.color = dest;
            matches++;
        }
        return matches;
    }

    private static string GetColorString(Color col)
    {
        return col.ToString();
    }
}
