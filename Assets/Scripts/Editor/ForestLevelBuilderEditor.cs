﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ForestLevelBuilder))]
public class ForestLevelBuilderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ForestLevelBuilder myScript = (ForestLevelBuilder)target;
        if (GUILayout.Button("Build Object"))
        {
            myScript.CreateLevel();
        }
    }

}
