﻿using System.Collections.Generic;
using UnityEngine;

public class ForestLevelBuilder : MonoBehaviour
{

    public static ForestLevelBuilder instance;

    public ForestBall ball;
    public List<float> ForestDifficultyParams;
    public GameObject pickupPrefab;
    public Transform pickupContainer;
    private List<PickUpItem> items;

    [Header("Boundries")]
    public Vector2 horizontalBoundries;
    public Vector2 verticalBoundries;

    private int numberOfPickups = -1;
    private int pickupsLeft = int.MaxValue;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        SetLevelParameters();
        CreateLevel();
    }

    private void Update()
    {
        UpdateDirectionHelper();
    }

    private void SetLevelParameters()
    {
        numberOfPickups = (int)ForestDifficultyParams[GameManager.instance.currentStressValue - 1];
        pickupsLeft = numberOfPickups;
    }

    public void CreateLevel()
    {
        float x, y;
        items = new List<PickUpItem>();

        for (int i = 0; i < numberOfPickups; i++)
        {
            x = Random.Range(horizontalBoundries.x, horizontalBoundries.y);
            y = Random.Range(verticalBoundries.x, verticalBoundries.y);
            GameObject go = Instantiate(pickupPrefab, new Vector3(x, y, -15), Quaternion.identity);
            go.transform.parent = pickupContainer;
            items.Add(go.GetComponent<PickUpItem>());
        }
    }

    internal int GetNumberOfPickups()
    {
        return numberOfPickups;
    }

    internal int GetNumberOfRemainingPickUps()
    {
        return pickupsLeft;
    }

    public void ItemDestroyed(PickUpItem pui)
    {
        items.Remove(pui);
        pickupsLeft--;
    }

    public void UpdateDirectionHelper()
    {
        if (pickupsLeft < 1)
            return;

        float closestDist = 1000;
        int closestIndex = -1;

        for (int i = 0; i < pickupsLeft; i++)
        {
            float dist = Vector3.Distance(items[i].transform.position, ball.transform.position);
            if(dist < closestDist)
            {
                closestDist = dist;
                closestIndex = i;
            }
        }

        Vector3 closestPos = items[closestIndex].transform.position;

        // set helper
        DirectionHelper.instance.ShowDirectionToPoint(closestPos);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(new Vector3(horizontalBoundries.x, verticalBoundries.x, -5), new Vector3(horizontalBoundries.y, verticalBoundries.x, -5)); // -
        Gizmos.DrawLine(new Vector3(horizontalBoundries.x, verticalBoundries.y, -5), new Vector3(horizontalBoundries.y, verticalBoundries.y, -5)); // _
        Gizmos.DrawLine(new Vector3(horizontalBoundries.x, verticalBoundries.x, -5), new Vector3(horizontalBoundries.x, verticalBoundries.y, -5)); // |_
        Gizmos.DrawLine(new Vector3(horizontalBoundries.y, verticalBoundries.x, -5), new Vector3(horizontalBoundries.y, verticalBoundries.y, -5)); // _|
    }
}
