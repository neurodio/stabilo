﻿//using System;
using UnityEngine;

public class PickUpItem : MonoBehaviour
{

    public GameObject destroyParticle;
    public LayerMask mask = -1;

    void Start()
    {
        PutOnGround(1);
    }

    void PutOnGround(int recurention)
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.forward, out hit, 100, mask.value))
        {
            //Debug.Log("tag: " + hit.collider.tag);
            if (hit.collider.CompareTag(Tags.ForestGround))
            {
                Debug.Log("onGround " + recurention);
                transform.position = hit.point - Vector3.forward;
            }
            else if (hit.collider.CompareTag(Tags.ForestDecoration)) // kamienie i inne modele
            {
                // antycollision code 
                // - regenerate position? - possible outofBound and infinite loops
                // - move to center (random distance) - infinite loops and position in center error
                
                float x, y;
                x = Random.Range(-3, 3);
                y = Random.Range(-3, 3);
                Debug.Log("Collide with decoration. Move: " + x + "  " + y);
                transform.position += new Vector3(x, y, 0);
                transform.position = new Vector3(transform.position.x, transform.position.y, -15);
                PutOnGround(recurention+1);
            }
        }

    }

    internal void Pickup()
    {
        SoundManager.instance.PlayOnPickupSfx();
        ForestLevelBuilder.instance.ItemDestroyed(this);
        GameObject go = Instantiate(destroyParticle);
        go.transform.position = transform.position;
        Destroy(gameObject);
    }
}
