﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionHelper : MonoBehaviour
{
    public static DirectionHelper instance;
    public Transform followGO;
    public Transform dir;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    void Update()
    {
        if (followGO != null)
        {
            this.transform.position = followGO.position;
        }
    }

    public void ShowDirectionToPoint(Vector3 pos)
    {
        pos.z = followGO.position.z;
        dir.LookAt(pos);
    }
}
