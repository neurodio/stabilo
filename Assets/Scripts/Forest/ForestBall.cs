﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestBall : MonoBehaviour
{
    public ParticleSystem waterParticle;
    public TrailRenderer trail;
    public bool touchingWater = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.PickupItem))
        {
            other.GetComponent<PickUpItem>().Pickup();
        }
        if (other.CompareTag(Tags.Water))
        {
            touchingWater = true;
            waterParticle.Play();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(Tags.Water))
        {
            touchingWater = false;
            waterParticle.Stop();
        }
    }
}
