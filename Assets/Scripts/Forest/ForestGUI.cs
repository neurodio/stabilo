﻿using UnityEngine;
using UnityEngine.UI;

public class ForestGUI : MonoBehaviour
{
    public Image progressImage;
    bool levelInProgress = false;

    int SparkPickUpCount = 0;

    public Text pointsText;


    internal void StartLevelGUI()
    {
        SparkPickUpCount = 0;
        pointsText.text = "0";
        levelInProgress = false;
        ShowTutorial();
    }

    private void ShowTutorial()
    {
        InGameMenu.instance.ShowTutorialPanel();
        TutorialManager.instance.ShowTutorial(Enums.GameType.Forest);
    }

    public void HideTutorial()
    {
        Debug.Log("Hide tutorial - forest");
        GameManager.instance.currentGameState = Enums.GameState.Level;
        Invoke("StartLevel", GameManager.instance.countdownSecounds);
    }

    private void StartLevel()
    {
        SparkPickUpCount = ForestLevelBuilder.instance.GetNumberOfPickups();
        levelInProgress = true;
    }

    private void Update()
    {
        if (levelInProgress && GameManager.instance.gameIsPaused == false)
        {
            // odpytuj ile objektów zostało
            int pickupsCount = ForestLevelBuilder.instance.GetNumberOfRemainingPickUps();
            // wylicz proporcje
            float progress = (float)pickupsCount / (float)SparkPickUpCount;
            SetProgressBar(progress);

            float points = (SparkPickUpCount - pickupsCount) * 10;
            pointsText.text = points.ToString();

            if (pickupsCount == 0)
            {
                GameManager.instance.ForestSetRecord((int)points);
                InGameMenu.instance.ShowWinPanel((int)points);
                levelInProgress = false;
            }
        }
    }

    internal void SetProgressBar(float life)
    {
        progressImage.fillAmount = life;
    }
}
