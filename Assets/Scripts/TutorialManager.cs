﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    public static TutorialManager instance;

    public GameObject holdInPosTutorial;
    public GameObject landscapeTutorial;
    public GameObject forestTutorial;

    public HoldPositionGUI hpGUI;
    public LandscapeRevealGUI lrGUI;
    public ForestGUI fGUI;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    public void ShowTutorial(Enums.GameType gameType)
    {
        if (gameType == Enums.GameType.HoldInPlace)
        {
            holdInPosTutorial.SetActive(true);
            landscapeTutorial.SetActive(false);
            forestTutorial.SetActive(false);
        }
        else if (gameType == Enums.GameType.Landscape)
        {
            holdInPosTutorial.SetActive(false);
            landscapeTutorial.SetActive(true);
            forestTutorial.SetActive(false);
        }
        else if (gameType == Enums.GameType.Forest)
        {
            holdInPosTutorial.SetActive(false);
            landscapeTutorial.SetActive(false);
            forestTutorial.SetActive(true);
        }
    }

    public void CloseAllTutorials()
    {

        if (GameManager.instance.currentGameType == Enums.GameType.HoldInPlace)
        {
            hpGUI.HideTutorial();
        }
        else if (GameManager.instance.currentGameType == Enums.GameType.Landscape)
        {
            lrGUI.HideTutorial();
        }
        else if (GameManager.instance.currentGameType == Enums.GameType.Forest)
        {
           fGUI.HideTutorial();
        }

        holdInPosTutorial.SetActive(false);
        landscapeTutorial.SetActive(false);
        forestTutorial.SetActive(false);
    }
}
