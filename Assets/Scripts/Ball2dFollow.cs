﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball2dFollow : MonoBehaviour {

    public Transform goToFollow;
    public float spriteHeight = -1;

    public List<Transform> nestedChildren;
    private List<Vector3> prevPositions;

	
	void Awake ()
    {

        prevPositions = new List<Vector3>();

    }
	

	void Update ()
    {
        if (GameManager.instance.gameIsPaused == true)
            return;

        Vector3 newPos = goToFollow.position;
        newPos.z = spriteHeight;
        transform.position = newPos;

        if(prevPositions.Count < nestedChildren.Count * 5)
        {
            prevPositions.Add(newPos);
        }
        else
        {
            prevPositions.RemoveAt(0);
            prevPositions.Add(newPos);
        }

        Vector3 refref = Vector3.zero;
        int numberOfSprites = nestedChildren.Count * 5;
        int numberOfPrevPositions = prevPositions.Count;

        if (numberOfPrevPositions >= numberOfSprites)
        {
            for (int i = 0; i < nestedChildren.Count; i++)
            {
                //nestedChildren[i].position = Vector3.SmoothDamp(nestedChildren[i].position, prevPositions[i], ref refref, 5);
                nestedChildren[i].position = prevPositions[numberOfPrevPositions - (i*3) - 1];
            }
        }

        transform.localScale = goToFollow.localScale;
	}
}
