﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public static CameraFollow instance;

    public Camera cam;
    public Quaternion startRotation;
    public Transform followGO;
    public float zOffset = 25;
    public float damp = 0.3f;
    public Vector3 moveOffset = Vector3.zero;
    private Vector3 velocity = Vector3.zero;


    public Enums.GameType gameType;
    public Boundries[] boundries;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        //startRotation = cam.transform.localRotation;
    }

    public void SetMoveOffset(Vector3 mo)
    {
        moveOffset = mo;
    }

    // Update is called once per frame
    void Update()
    {
        //good
        if (followGO != null)
        {
            Vector3 targetPosition = followGO.position + new Vector3(0, 0, zOffset) + moveOffset * 5;
            Vector3 newPosition = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, damp);
            if (!boundries[(int)gameType].useBoundries)
                transform.position = newPosition;
            else
            {
                if (newPosition.x < boundries[(int)gameType].horizontalBoundries.x)
                    newPosition.x = boundries[(int)gameType].horizontalBoundries.x;
                else if (newPosition.x > boundries[(int)gameType].horizontalBoundries.y)
                    newPosition.x = boundries[(int)gameType].horizontalBoundries.y;

                if (newPosition.y < boundries[(int)gameType].verticalBoundries.x)
                    newPosition.y = boundries[(int)gameType].verticalBoundries.x;
                else if (newPosition.y > boundries[(int)gameType].verticalBoundries.y)
                    newPosition.y = boundries[(int)gameType].verticalBoundries.y;

                transform.position = newPosition;
            }
        }

        //cam.transform.LookAt(followGO.position);
        //Quaternion q = cam.transform.rotation;
        //q.y = 0;
        //cam.transform.rotation = q;

        //var lookPos = followGO.position;// - transform.position;
        //lookPos.x = 0;
        //lookPos.z = 0;
        //lookPos.y = 0;
        //cam.transform.LookAt(lookPos);
        //var rotation = Quaternion.LookRotation(lookPos);
        // cam.transform.rotation = Quaternion.Slerp(cam.transform.rotation, rotation, Time.deltaTime * 1);



        //if(moveOffset.x > 0)
        //cam.transform.localRotation = Quaternion.Euler(startRotation.eulerAngles + (new Vector3( moveOffset.y, moveOffset.x, 0) * 30));
    }

    public void SetFollowObject(Transform f)
    {
        followGO = f;

        if (GameManager.instance.currentGameType == Enums.GameType.Landscape)
        {
            float hmax = 15;
            float hmin = 11;
            float h = 0;
            h = Mathf.Lerp(hmin, hmax,(float)GameManager.instance.currentStressValue / 9f);
            boundries[(int)gameType].horizontalBoundries.x = -h;
            boundries[(int)gameType].horizontalBoundries.y = h;

            float vmax = 5;
            float vmin = 3;
            float v = Mathf.Lerp(vmin, vmax, (float)GameManager.instance.currentStressValue / 9f);
            boundries[(int)gameType].verticalBoundries.y = v;
            boundries[(int)gameType].verticalBoundries.x = -v + 2;
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(new Vector3(boundries[(int)gameType].horizontalBoundries.x, boundries[(int)gameType].verticalBoundries.x, -5), new Vector3(boundries[(int)gameType].horizontalBoundries.y, boundries[(int)gameType].verticalBoundries.x, -5)); // -
        Gizmos.DrawLine(new Vector3(boundries[(int)gameType].horizontalBoundries.x, boundries[(int)gameType].verticalBoundries.y, -5), new Vector3(boundries[(int)gameType].horizontalBoundries.y, boundries[(int)gameType].verticalBoundries.y, -5)); // _
        Gizmos.DrawLine(new Vector3(boundries[(int)gameType].horizontalBoundries.x, boundries[(int)gameType].verticalBoundries.x, -5), new Vector3(boundries[(int)gameType].horizontalBoundries.x, boundries[(int)gameType].verticalBoundries.y, -5)); // |_
        Gizmos.DrawLine(new Vector3(boundries[(int)gameType].horizontalBoundries.y, boundries[(int)gameType].verticalBoundries.x, -5), new Vector3(boundries[(int)gameType].horizontalBoundries.y, boundries[(int)gameType].verticalBoundries.y, -5)); // _|
    }
}
