﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Enums
{
    public enum GameState { MainMenu, Level, GameOver, Tutorial}
    public enum CanvasAnimationDir { Open, Close}
    public enum GameType { HoldInPlace, Landscape, Forest }
    public enum SoundType { music, sfx}
}

public static class Tags
{
    public static string Spikes = "Spikes";
    public static string Player = "Player";
    public static string ForestGround = "ForestGround";
    public static string PickupItem = "PickupItem";
    public static string Water = "Water";
    public static string ForestDecoration = "ForestDecoration";
}