﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundChecker : MonoBehaviour
{

    public Enums.SoundType soundType;

    private AudioSource mas;

    void Awake()
    {
        mas = GetComponent<AudioSource>();
    }

    void Start()
    {
        SoundManager.instance.AddSoundChecker(this);
        UpdateSettings(SoundManager.instance.musicIsOn, SoundManager.instance.sfxIsOn);
    }


    void OnDisable()
    {
        SoundManager.instance.DeleteSoundChecker(this);
    }

    internal void UpdateSettings(bool musicIsOn, bool sfxIsOn)
    {
        if (soundType == Enums.SoundType.music)
        {
            if (musicIsOn)
                mas.Play();
            else
                mas.Stop();
        }
        else
        {
            if (sfxIsOn)
                mas.Play();
            else
                mas.Stop();
        }
    }
}
