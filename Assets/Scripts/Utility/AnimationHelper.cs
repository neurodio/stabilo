﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AnimationHelper : MonoBehaviour
{
    public static AnimationHelper instance;
    
    [Header("Animations")]
    public float OpenAnimTime = 1;
    public float CloseAnimTime = 0.7f;
    private bool animationInProgress = false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
            Destroy(gameObject);
    }

    public void AnimateCanvas(Enums.CanvasAnimationDir animDir, CanvasGroup canvasGroup)
    {
        if (animationInProgress == false)
        {
            animationInProgress = true;
            StartCoroutine(AnimateCanvasCoroutine(animDir, canvasGroup));
        }
        else
            Debug.LogError("Animacja pominięta: " + animDir + "  " + canvasGroup.gameObject.name);
    }

    IEnumerator AnimateCanvasCoroutine(Enums.CanvasAnimationDir animDir, CanvasGroup canvasGroup)
    {
        float timer = 0;
        float animTime, start, end = 0;
        if (animDir == Enums.CanvasAnimationDir.Open)
        {
            canvasGroup.gameObject.SetActive(true);
            animTime = OpenAnimTime;
            start = 0;
            end = 1;
        }
        else
        {
            animTime = CloseAnimTime;
            start = 1;
            end = 0;
        }

        while (timer < animTime)
        {
            timer += Time.fixedDeltaTime;
            canvasGroup.alpha = Mathf.Lerp(start, end, timer / animTime);
            yield return null;
        }

        canvasGroup.alpha = end;
        if (animDir == Enums.CanvasAnimationDir.Close)
            canvasGroup.gameObject.SetActive(false);
        animationInProgress = false;
    }
}
