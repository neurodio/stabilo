﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class HoldPositionGUI : MonoBehaviour
{

    public Text timeText;
    private float timeToCountdown; // sec
    private bool isTicking = false;

    public Image lifeImage;
    public Text pointsText;
    private int points = 0;
    private float pointsUpdateTime = 0.2f;
    private float pointsTimer = 0;

    public void StartLevelGUI(float secounds)
    {
        isTicking = false;
        pointsText.text = "0";
        points = 0;
        pointsTimer = 0;
        timeToCountdown = secounds;
        DateTime dt1 = new DateTime();
        DateTime dt = dt1.AddSeconds(timeToCountdown);
        timeText.text = dt.Minute.ToString("00") + ":" + dt.Second.ToString("00") + ":" + dt.Millisecond.ToString("00").Substring(0, 2);
        ShowTutorial();
    }

    private void ShowTutorial()
    {
        InGameMenu.instance.ShowTutorialPanel();
        TutorialManager.instance.ShowTutorial(Enums.GameType.HoldInPlace);
    }

    public void HideTutorial()
    {
        Debug.Log("Hide tutorial - holdPos");
        GameManager.instance.currentGameState = Enums.GameState.Level;
        Invoke("StartLevel", GameManager.instance.countdownSecounds);
    }

    private void StartLevel()
    {
        isTicking = true;
    }

    void Update()
    {
        if (isTicking && GameManager.instance.gameIsPaused == false)
        {
            timeToCountdown -= Time.deltaTime;
            if (timeToCountdown <= 0.0f)
            {
                isTicking = false;
                timeText.text = "00:00:00";
                // koniec gry
                GameManager.instance.HoldPosSetRecord(points);
                InGameMenu.instance.ShowWinPanel(points);
            }
            else
            {
                DateTime dt1 = new DateTime();
                DateTime dt = dt1.AddSeconds(timeToCountdown);
                timeText.text = dt.Minute.ToString("00") + ":" + dt.Second.ToString("00") + ":" + dt.Millisecond.ToString("00").Substring(0, 2);
            }

            pointsTimer += Time.deltaTime;
            if (pointsTimer >= pointsUpdateTime)
            {
                pointsTimer = 0;
                float dist = HoldPositionBall.instance.GetDistFromCenter();
                points += Mathf.RoundToInt(10 - dist);
                Debug.Log("Points " + points + "  dodano: " + Mathf.RoundToInt(10 - dist));
                pointsText.text = points.ToString();
            }
        }
    }


    internal void SetLife(float life)
    {
        if (life <= 0)
            isTicking = false;
        lifeImage.fillAmount = life;
    }
}
