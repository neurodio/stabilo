﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikesShapes : MonoBehaviour {

    public Transform ballPos;
    SkinnedMeshRenderer skinnedMeshRenderer;
    private float dist = 0;
    public Material spikesMat;
    public Color nearColor;
    public Color farColor;

    // Use this for initialization
    void Start () {
        skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
        skinnedMeshRenderer.materials[0] = new Material(spikesMat);
    }
	
	// Update is called once per frame
	void Update ()
    {
        dist = Vector3.Distance(this.transform.position, ballPos.position);
        skinnedMeshRenderer.SetBlendShapeWeight(0, 100 - (20 * dist));
        skinnedMeshRenderer.materials[0].color = Color.Lerp(nearColor, farColor, dist/5);
	}
}
