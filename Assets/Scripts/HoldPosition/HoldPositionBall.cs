﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldPositionBall : MonoBehaviour
{
    public static HoldPositionBall instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(Tags.Spikes))
        {
            GameManager.instance.GetHit(1);
        }
    }

    public float GetDistFromCenter()
    {
        return Vector3.Distance( transform.position, Vector3.zero);
    }
}
