﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomForce : MonoBehaviour
{
    public static RandomForce instance;

    public Rigidbody ballRigid;
    public float newForceTime = 5;
    public float forceTime = 10;
    public float minimalForce = 0.5f;
    public float maxForce = 2;

    float timer = 7;

    Vector3 forceVector;
    float randomForceMultiplier = 1;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        timer = forceTime - GameManager.instance.countdownSecounds;
    }
    
    void FixedUpdate ()
    {
        if (GameManager.instance.currentGameState == Enums.GameState.Level)
        {
            if (timer < forceTime)
            {
                timer += Time.fixedDeltaTime;

            }
            else
            {
                timer = 0;
                RandomizeForces();
            }
        }
		//if(GameManager.instance.gameIsPaused == false)
  //      {
  //          //Debug.Log("AddForce: " + (forceVector * randomForceMultiplier).ToString());
  //          //ballRigid.AddForce(forceVector * randomForceMultiplier);
  //      }
	}

    void RandomizeForces()
    {
        float x = Random.Range(-1f, 1);
        float y = Random.Range(-1f, 1);
        forceVector = new Vector3(x, y, 0);
        randomForceMultiplier = Random.Range(minimalForce, maxForce);
        //Debug.Log("RandomizeForces: " + x + " " + y + "    " + randomForceMultiplier);
    }

    public Vector3 GetRandomForceVector()
    {
        return forceVector * randomForceMultiplier;
    }
}
