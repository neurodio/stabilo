﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    [Header("Music")]
    public AudioSource musicAS;
    public bool musicIsOn = true;
    public Image musicImage;
    public Sprite musicOn;


    public Sprite musicOff;

    [Header("Sfx")]
    public AudioSource sfxAS;
    public bool sfxIsOn = true;
    public Image sfxImage;
    public Sprite sfxOn;
    public Sprite sfxOff;

    [Space(10)]
    public List<SoundChecker> soundCheckers;
    public AudioClip MenuClick;
    public AudioClip WinPanelShowup;
    public AudioClip Pop;
    public AudioClip Pickup;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
            Destroy(gameObject);


        musicIsOn = PlayerPrefs.GetInt("music", 1) == 1 ? true : false;
        sfxIsOn = PlayerPrefs.GetInt("sfx", 1) == 1 ? true : false;
        soundCheckers = new List<SoundChecker>();
        UpdateSoundButtons();
    }

    void Start()
    {
        if (musicIsOn)
            musicAS.Play();
    }

    void SaveSettings()
    {
        PlayerPrefs.SetInt("music", musicIsOn ? 1 : 0);
        PlayerPrefs.SetInt("sfx", sfxIsOn ? 1 : 0);
    }

    public void ChangeMusicState()
    {
        musicIsOn = !musicIsOn;

        if (musicIsOn)
            musicAS.Play();
        else
            musicAS.Stop();

        UpdateSoundButtons();
    }

    public void ChangeSfsState()
    {
        sfxIsOn = !sfxIsOn;
        UpdateSoundButtons();
    }

    private void UpdateSoundButtons()
    {
        if (musicIsOn)
            musicImage.sprite = musicOn;
        else
            musicImage.sprite = musicOff;

        if (sfxIsOn)
            sfxImage.sprite = sfxOn;
        else
            sfxImage.sprite = sfxOff;

        UpdateOtherSources();
        SaveSettings();
    }

    private void UpdateOtherSources()
    {
        for(int i = 0; i < soundCheckers.Count; i++)
        {
            soundCheckers[i].UpdateSettings(musicIsOn, sfxIsOn);
        }
    }

    
    public void AddSoundChecker(SoundChecker soundChecker)
    {
        soundCheckers.Add(soundChecker);
    }

    public  void DeleteSoundChecker(SoundChecker soundChecker)
    {
        soundCheckers.Remove(soundChecker);
    }

    public bool SfxCheck()
    {
        return sfxIsOn;
    }

    public void PlayOnClickSfx()
    {
        sfxAS.pitch = 1;
        if (sfxIsOn)
            sfxAS.PlayOneShot(MenuClick);
    }

    public void PlayOnShowWinSfx()
    {
        sfxAS.pitch = 1;
        if (sfxIsOn)
            sfxAS.PlayOneShot(WinPanelShowup);
    }

    public void PlayOnPopSfx()
    {
        if (sfxIsOn)
        {
            sfxAS.pitch = UnityEngine.Random.Range(0, 2.0f);
            sfxAS.PlayOneShot(Pop);
            //sfxAS.pitch = 1;
        }
    }

    public void PlayOnPickupSfx()
    {
        if (sfxIsOn)
        {
            sfxAS.pitch = 1;
            sfxAS.PlayOneShot(Pickup);
        }
    }

    public void ChangeSong(AudioClip newSong)
    {
        StartCoroutine(SongChanger(newSong));
    }

    IEnumerator SongChanger(AudioClip newSong)
    {
        if(musicIsOn == false)
            yield break;
        
        Debug.Log("SongChanger");
        float changeSongTimer = GameManager.instance.changeSongTimer;
        float timer = 0;

        while(timer < changeSongTimer)
        {
            timer +=Time.deltaTime;

            musicAS.volume = Mathf.Lerp(1, 0, timer/changeSongTimer);
            yield return null;
        }

        musicAS.Stop();
        musicAS.clip = newSong;
        musicAS.Play();

        timer = 0;
        while(timer < changeSongTimer)
        {
            timer +=Time.deltaTime;

            musicAS.volume = Mathf.Lerp(0, 1, timer/changeSongTimer);
            yield return null;
        }
    }
}