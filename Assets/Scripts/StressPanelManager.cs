﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StressPanelManager : MonoBehaviour
{

    [Header("Stress panel")]
    public Slider stressSlider;
    public Image faceBackImage;
    public Image faceImage;
    public List<Sprite> facesSprites;
    public Color smileColor;
    public Color neutralColor;
    public Color sadColor;

    public void OnEnable()
    {
        stressSlider.value = Mathf.FloorToInt((float)stressSlider.maxValue / 2) + 1;
        faceImage.sprite = facesSprites[(int)stressSlider.value - 1];
        SetFaceColor();
    }

    public void StressSliderUpdate()
    {
        GameManager.instance.SetStressValue(stressSlider.value);
        faceImage.sprite = facesSprites[(int)stressSlider.value - 1];
        SetFaceColor();
    }

    private void SetFaceColor()
    {
        float progress = stressSlider.value / (stressSlider.maxValue - stressSlider.minValue + 1);
        if (progress < 0.4f)
            faceBackImage.color = Color.Lerp(smileColor, neutralColor, progress);
        else if (progress >= 0.4f && progress <= 0.6f)
            faceBackImage.color = neutralColor;
        else
            faceBackImage.color = Color.Lerp(neutralColor, sadColor, progress);
    }

    public void CloseSliderPanel()
    {
        MainMenuManager.instance.CloseStressPanel_Anim();
    }

    public void StartGame()
    {
        GameManager.instance.StartNewGame();
    }
}
