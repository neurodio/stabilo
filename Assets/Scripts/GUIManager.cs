﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour
{

    public static GUIManager instance;

    [Header("Main Panels")]
    public CanvasGroup MainMenuPanel;
    public CanvasGroup InGameMenuPanel;
    public GameObject MainMenuParticles;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
            Destroy(gameObject);
    }

    public void CloseGame()
    {
        Application.Quit();
    }

    #region MainMenuPanel

    public void OpenMainMenuPanel_Anim()
    {
        MainMenuParticles.SetActive(true);
        AnimationHelper.instance.AnimateCanvas(Enums.CanvasAnimationDir.Open, MainMenuPanel);
        MainMenuManager.instance.CloseStressPanel();
        CloseInGamePanel();
    }

    public void OpenMainMenuPanel()
    {
        MainMenuParticles.SetActive(true);
        MainMenuPanel.alpha = 1;
        MainMenuManager.instance.CloseStressPanel();
        CloseInGamePanel();
    }

    public void CloseMainMenuPanel()
    {
        MainMenuParticles.SetActive(false);
        MainMenuPanel.gameObject.SetActive(false);
    }

    #endregion MainMenuPanel

    #region InGamePanel

    public void OpenInGamePanel_Anim()
    {
        CloseMainMenuPanel();
        InGameMenuPanel.gameObject.SetActive(true);
        InGameMenuPanel.alpha = 1;
        InGameMenu.instance.OpenGamePanel_Anim();
    }

    public void OpenInGamePanel()
    {
        CloseMainMenuPanel();
        InGameMenuPanel.gameObject.SetActive(true);
        InGameMenuPanel.alpha = 1;
        InGameMenu.instance.ClosePausePanel();
    }

    public void CloseInGamePanel_Anim()
    {
        InGameMenu.instance.ClosePausePanel();
        AnimationHelper.instance.AnimateCanvas(Enums.CanvasAnimationDir.Close, InGameMenuPanel);
    }

    public void CloseInGamePanel()
    {
        InGameMenu.instance.ClosePausePanel();
        InGameMenuPanel.gameObject.SetActive(false);
    }

    #endregion InGamePanel

}
