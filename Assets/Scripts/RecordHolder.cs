﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecordHolder : MonoBehaviour
{
    public Text holdText;
    public Text landscapText;
    public Text forestText;

    private void OnEnable()
    {
        int h, l, f;
        h = GameManager.instance.holdRecord;
        l = GameManager.instance.landscapeRecord;
        f = GameManager.instance.forestRecord;

        holdText.text = h == 0 ? "---" : h.ToString();
        landscapText.text = l == 0 ? "---" : l.ToString();
        forestText.text = f == 0 ? "---" : f.ToString();
    }
}
