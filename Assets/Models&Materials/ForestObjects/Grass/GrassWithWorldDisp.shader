﻿Shader "Grass/GrassWithWorldDisp"
{
	//https://c10.patreonusercontent.com/3/e30%3D/patreon-posts/bBAaPd33AQY2h29UHus5IqZhspiIN3yXybmCMEkEgqnGumfcC1mcxAr1iWmGzLUS.gif?token-time=1532649600&token-hash=piNSXLoJ05IS7auS3ULMRxlEKvyrRu-cCNP6O_QZY2c%3D
	Properties 
	{
        _Color ("Main Color", Color) = (0.5,0.5,0.5,1)
        _MainTex ("Base (RGB)", 2D) = "white" {}
		_Ramp ("Toon Ramp (RGB)", 2D) = "gray" {}

        _Speed ("MoveSpeed", Range(20,50)) = 25 // speed of the swaying
        _SwayMax("Sway Max", Range(0, 0.1)) = .005 // how far the swaying goes

		_Radius("Radius", Range(0,10)) = 1 // width of the line around the dissolve
		_MaxDisplacement("Max discpacement", Range(0,100)) = 1
		_SmashDistance("Smash distance", Range(0,3)) = 0.5
    }
 
    SubShader 
	{
        Tags { "RenderType"="Opaque" "DisableBatching" = "True" }// disable batching lets us keep object space
        LOD 200
        Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
       
		CGPROGRAM
		// addshadow applies shadow after vertex animation
		#pragma surface surf ToonRamp vertex:vert addshadow keepalpha 
		
		sampler2D _Ramp;
		
		// custom lighting function that uses a texture ramp based
		// on angle between light direction and normal
		#pragma lighting ToonRamp exclude_path:prepass
		inline half4 LightingToonRamp (SurfaceOutput s, half3 lightDir, half atten)
		{
			#ifndef USING_DIRECTIONAL_LIGHT
			lightDir = normalize(lightDir);
			#endif
		
			half d = dot (s.Normal, lightDir)*0.5 + 0.5;
			half3 ramp = tex2D (_Ramp, float2(d,d)).rgb;
		
			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * ramp * (atten * 2);
			c.a = s.Alpha;
			return c;
		}
		
		sampler2D _MainTex;
		float4 _Color;
		float _Radius;
		float _MaxDisplacement;
		float _SmashDistance;
		
		float _Speed;
		float _SwayMax;
		
		uniform float3 _BallWorldPosition;
	
		struct Input 
		{
			float2 uv_MainTex : TEXCOORD0;
		};

		void vert(inout appdata_full v)
		{
			// BASIC SWAYING MOVEMENT
			float3 wpos = mul(unity_ObjectToWorld, v.vertex).xyz;// world position vertexa
			float x = sin(wpos.x + (_Time.x * _Speed)) *(v.vertex.y) * 5;// x axis movements
			float z = sin(wpos.z + (_Time.x * _Speed)) *(v.vertex.y) * 5;// z axis movements
			// step(edge, x) --> returns 0.0 when x < edge, returns 1.0 otherwise
			v.vertex.x += (step(0,v.vertex.y) * x * _SwayMax);// apply the movement if the vertex's y above the YOffset
			v.vertex.z += (step(0,v.vertex.y) * z * _SwayMax);
			

			// BALL DISPLACEMENT
			float ballDistance =  distance(_BallWorldPosition.xy, wpos.xy); // distance for radius
			float2 ballDisplacement = wpos.xy - _BallWorldPosition.xy; // position comparison
			float ySquare = abs(v.vertex.z)  * 0.01;

			if(ballDistance < _Radius && ballDistance > _SmashDistance)
			{
				float Pidiv2 = 1.57079632;
				float normalizeBallDist = ballDistance / _Radius;
				v.vertex.x += (_MaxDisplacement * ySquare * -ballDisplacement.x) * (2*cos(normalizeBallDist*Pidiv2));
				v.vertex.z += (_MaxDisplacement * ySquare * -ballDisplacement.y) * (2*cos(normalizeBallDist*Pidiv2));
				/// v.vertex.x += ((_MaxDisplacement * ySquare * ballDisplacement.x) * 0.01) * cos(normalizeBallDist);
				/// v.vertex.z += ((_MaxDisplacement * ySquare * ballDisplacement.y) * 0.01) * cos(normalizeBallDist);
				v.vertex.y -= (_MaxDisplacement  * ySquare) * (2*cos(normalizeBallDist*Pidiv2));
			}
			else if(ballDistance <= _SmashDistance)
			{
				v.vertex.y = -5;
			}
		}
	
		void surf (Input IN, inout SurfaceOutput o) 
		{
			half4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}

		ENDCG
 
    }
 
    Fallback "Diffuse"
}
