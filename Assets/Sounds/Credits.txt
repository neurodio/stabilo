Title: Item Pickup
Author: TreasureSounds
Source: https://freesound.org/s/332629/
License: http://creativecommons.org/licenses/by/3.0/
Copyright Free Music found @ https://freesound.org


Title: oceanwavescrushing.wav
Author: Luftrum
Source: https://freesound.org/s/48412/
License: http://creativecommons.org/licenses/by/3.0/
Copyright Free Music found @ https://freesound.org


Title: Pop (made by DuffyBro)
Author: DuffyBro
Source: https://freesound.org/s/319107/
License: http://creativecommons.org/licenses/by/3.0/
Copyright Free Music found @ https://freesound.org


Licencja wszystkich: Attribution 4.0 International (CC BY 4.0) 

//Menu music

Title: Remember Trees? 
Author: Chris Zabriskie 
Source: http://freemusicarchive.org/music/Chris_Zabriskie/Music_from_Neptune_Flux/ChrisZabriskie-MusicfromNeptuneFlux-07
License: https://creativecommons.org/licenses/by/4.0/
Copyright Chris Zabriskie

//Hold position music

Title: Oxygen Garden
Author: Chris Zabriskie
Source: http://freemusicarchive.org/music/Chris_Zabriskie/Divider/05_-_Oxygen_Garden
License: https://creativecommons.org/licenses/by/4.0/
Copyright Chris Zabriskie

//Landscape music
 
Title: Wonder Cycle
Author: Chris Zabriskie
Source: http://freemusicarchive.org/music/Chris_Zabriskie/Divider/04_-_Wonder_Cycle
License: https://creativecommons.org/licenses/by/4.0/
Copyright Chris Zabriskie

//Forest music

Title: Candlepower
Author: Chris Zabriskie
Source: http://freemusicarchive.org/music/Chris_Zabriskie/Divider/02_-_Candlepower
License: https://creativecommons.org/licenses/by/4.0/
Copyright Chris Zabriskie 